import dramatiq
import logging
from datetime import datetime

logger = logging.getLogger(__name__)

@dramatiq.actor
def minutely_tasks():
    logger.warning(datetime.now())
